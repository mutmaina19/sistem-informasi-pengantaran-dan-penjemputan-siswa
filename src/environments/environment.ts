// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCHAF_W3LJmYtCYzZLWasGmVznv3eW8HZA",
    authDomain: "pengantaran-dan-penjemputan.firebaseapp.com",
    projectId: "pengantaran-dan-penjemputan",
    storageBucket: "pengantaran-dan-penjemputan.appspot.com",
    messagingSenderId: "717400460954",
    appId: "1:717400460954:web:0e559df3535031890c1923"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
