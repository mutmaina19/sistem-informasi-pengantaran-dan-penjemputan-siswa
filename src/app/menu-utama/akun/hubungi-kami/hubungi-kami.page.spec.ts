import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HubungiKamiPage } from './hubungi-kami.page';

describe('HubungiKamiPage', () => {
  let component: HubungiKamiPage;
  let fixture: ComponentFixture<HubungiKamiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HubungiKamiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HubungiKamiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
