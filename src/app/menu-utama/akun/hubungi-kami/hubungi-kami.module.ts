import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HubungiKamiPageRoutingModule } from './hubungi-kami-routing.module';

import { HubungiKamiPage } from './hubungi-kami.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HubungiKamiPageRoutingModule
  ],
  declarations: [HubungiKamiPage]
})
export class HubungiKamiPageModule {}
