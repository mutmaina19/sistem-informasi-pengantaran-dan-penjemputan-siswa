import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InformasiAkunPageRoutingModule } from './informasi-akun-routing.module';

import { InformasiAkunPage } from './informasi-akun.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InformasiAkunPageRoutingModule
  ],
  declarations: [InformasiAkunPage]
})
export class InformasiAkunPageModule {}
