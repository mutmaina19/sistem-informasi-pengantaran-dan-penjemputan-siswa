import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InformasiAkunPage } from './informasi-akun.page';

const routes: Routes = [
  {
    path: '',
    component: InformasiAkunPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InformasiAkunPageRoutingModule {}
