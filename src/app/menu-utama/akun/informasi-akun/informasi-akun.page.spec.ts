import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InformasiAkunPage } from './informasi-akun.page';

describe('InformasiAkunPage', () => {
  let component: InformasiAkunPage;
  let fixture: ComponentFixture<InformasiAkunPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformasiAkunPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InformasiAkunPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
