import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PenilaianPage } from './penilaian.page';

describe('PenilaianPage', () => {
  let component: PenilaianPage;
  let fixture: ComponentFixture<PenilaianPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenilaianPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PenilaianPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
