import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AkunPage } from './akun.page';

const routes: Routes = [
  {
    path: '',
    component: AkunPage
  },
  {
    path: 'informasi-akun',
    loadChildren: () => import('./informasi-akun/informasi-akun.module').then( m => m.InformasiAkunPageModule)
  },
  {
    path: 'penilaian',
    loadChildren: () => import('./penilaian/penilaian.module').then( m => m.PenilaianPageModule)
  },
  {
    path: 'pengaturan',
    loadChildren: () => import('./pengaturan/pengaturan.module').then( m => m.PengaturanPageModule)
  },
  {
    path: 'tentang-kami',
    loadChildren: () => import('./tentang-kami/tentang-kami.module').then( m => m.TentangKamiPageModule)
  },
  {
    path: 'hubungi-kami',
    loadChildren: () => import('./hubungi-kami/hubungi-kami.module').then( m => m.HubungiKamiPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AkunPageRoutingModule {}
