import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MetodePembayaranPage } from './metode-pembayaran.page';

describe('MetodePembayaranPage', () => {
  let component: MetodePembayaranPage;
  let fixture: ComponentFixture<MetodePembayaranPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetodePembayaranPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MetodePembayaranPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
