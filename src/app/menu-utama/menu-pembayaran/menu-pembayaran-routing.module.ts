import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuPembayaranPage } from './menu-pembayaran.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPembayaranPage
  },
  {
    path: 'pembayaran-utama',
    loadChildren: () => import('./pembayaran-utama/pembayaran-utama.module').then( m => m.PembayaranUtamaPageModule)
  },
  {
    path: 'voucher',
    loadChildren: () => import('./voucher/voucher.module').then( m => m.VoucherPageModule)
  },
  {
    path: 'metode-pembayaran',
    loadChildren: () => import('./metode-pembayaran/metode-pembayaran.module').then( m => m.MetodePembayaranPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuPembayaranPageRoutingModule {}
