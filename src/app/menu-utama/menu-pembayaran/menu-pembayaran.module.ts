import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenuPembayaranPageRoutingModule } from './menu-pembayaran-routing.module';

import { MenuPembayaranPage } from './menu-pembayaran.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuPembayaranPageRoutingModule
  ],
  declarations: [MenuPembayaranPage]
})
export class MenuPembayaranPageModule {}
