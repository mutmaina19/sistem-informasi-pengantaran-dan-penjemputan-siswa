import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MenuPembayaranPage } from './menu-pembayaran.page';

describe('MenuPembayaranPage', () => {
  let component: MenuPembayaranPage;
  let fixture: ComponentFixture<MenuPembayaranPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuPembayaranPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MenuPembayaranPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
