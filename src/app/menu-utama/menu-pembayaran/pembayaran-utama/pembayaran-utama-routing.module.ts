import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PembayaranUtamaPage } from './pembayaran-utama.page';

const routes: Routes = [
  {
    path: '',
    component: PembayaranUtamaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PembayaranUtamaPageRoutingModule {}
