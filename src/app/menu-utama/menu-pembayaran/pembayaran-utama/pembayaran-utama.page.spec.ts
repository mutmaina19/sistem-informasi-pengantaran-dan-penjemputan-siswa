import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PembayaranUtamaPage } from './pembayaran-utama.page';

describe('PembayaranUtamaPage', () => {
  let component: PembayaranUtamaPage;
  let fixture: ComponentFixture<PembayaranUtamaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PembayaranUtamaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PembayaranUtamaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
