import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PembayaranUtamaPageRoutingModule } from './pembayaran-utama-routing.module';

import { PembayaranUtamaPage } from './pembayaran-utama.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PembayaranUtamaPageRoutingModule
  ],
  declarations: [PembayaranUtamaPage]
})
export class PembayaranUtamaPageModule {}
