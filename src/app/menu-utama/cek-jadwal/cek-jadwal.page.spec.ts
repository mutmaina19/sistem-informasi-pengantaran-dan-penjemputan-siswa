import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CekJadwalPage } from './cek-jadwal.page';

describe('CekJadwalPage', () => {
  let component: CekJadwalPage;
  let fixture: ComponentFixture<CekJadwalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CekJadwalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CekJadwalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
