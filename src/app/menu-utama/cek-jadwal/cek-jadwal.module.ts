import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CekJadwalPageRoutingModule } from './cek-jadwal-routing.module';

import { CekJadwalPage } from './cek-jadwal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CekJadwalPageRoutingModule
  ],
  declarations: [CekJadwalPage]
})
export class CekJadwalPageModule {}
