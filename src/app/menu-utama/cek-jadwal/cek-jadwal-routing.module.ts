import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CekJadwalPage } from './cek-jadwal.page';

const routes: Routes = [
  {
    path: '',
    component: CekJadwalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CekJadwalPageRoutingModule {}
