import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PemesananPage } from './pemesanan.page';

describe('PemesananPage', () => {
  let component: PemesananPage;
  let fixture: ComponentFixture<PemesananPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PemesananPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PemesananPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
