import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InfoBusPageRoutingModule } from './info-bus-routing.module';

import { InfoBusPage } from './info-bus.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InfoBusPageRoutingModule
  ],
  declarations: [InfoBusPage]
})
export class InfoBusPageModule {}
