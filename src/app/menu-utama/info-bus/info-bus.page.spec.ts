import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InfoBusPage } from './info-bus.page';

describe('InfoBusPage', () => {
  let component: InfoBusPage;
  let fixture: ComponentFixture<InfoBusPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoBusPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InfoBusPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
