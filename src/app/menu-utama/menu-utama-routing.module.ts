import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuUtamaPage } from './menu-utama.page';

const routes: Routes = [
  {
    path: '',
    component: MenuUtamaPage
  },
  {
    path: 'info-bus',
    loadChildren: () => import('./info-bus/info-bus.module').then( m => m.InfoBusPageModule)
  },
  {
    path: 'cek-jadwal',
    loadChildren: () => import('./cek-jadwal/cek-jadwal.module').then( m => m.CekJadwalPageModule)
  },
  {
    path: 'pemesanan',
    loadChildren: () => import('./pemesanan/pemesanan.module').then( m => m.PemesananPageModule)
  },
  {
    path: 'menu-pembayaran',
    loadChildren: () => import('./menu-pembayaran/menu-pembayaran.module').then( m => m.MenuPembayaranPageModule)
  },
  {
    path: 'akun',
    loadChildren: () => import('./akun/akun.module').then( m => m.AkunPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenuUtamaPageRoutingModule {}
