import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { AngularFireAuth} from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import firebase from 'firebase/app';


interface User{
  name: string;
  email: string;
}
@Component({
  selector: 'app-daftar',
  templateUrl: './daftar.page.html',
  styleUrls: ['./daftar.page.scss'],
})
export class DaftarPage implements OnInit {

user: any ={};
userData: User;

  constructor(
    public toastController: ToastController,
    public auth: AngularFireAuth,
    public db: AngularFirestore,
    public router: Router
    
  ) { }

  ngOnInit() {
  } 
  async pesankesalahan() {
    const toast = await this.toastController.create({
        message: 'Tidak dapat melakukan registrasi',
        duration: 2000,
        position: 'middle'
    });
    toast.present();
}
  loading: boolean;
daftar()
{
this.loading = true;
this.auth.createUserWithEmailAndPassword(this.user.email, this.user.password). then(res=>{
this.loading = false;
this.writeUser(res.user.email);
}, err=>{
  this.pesankesalahan();
  this.loading = false;
})
}

writeUser(email)
{
  this.userData = {
    name: this.user.fullname,
    email: this.user.email
};
  this.db.collection('users').doc (email).set(this.userData).then(res=>{
    this.router.navigate(['/login']);
}, err => {
    console.log(err);
    this.pesankesalahan();
    this.loading = false;
   
  })
    
}
}
